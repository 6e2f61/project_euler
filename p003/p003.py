"""
Problem 3
Largest prime factor

The prime factors of 13195 are 5, 7, 13 and 29.
What is the largest prime factor of the number 600851475143 ?
"""

from datetime import datetime


def is_prime(number):
    if number <= 1:
        return False
    for i in range(2, int(number ** 0.5) + 1):
        if not number % i:
            return False
    return True


def max_prime_factor(number):
    foo = number
    bar = 1
    eggs = []
    while bar <= foo:
        for i in range(2, foo + 1):
            if not foo % i and is_prime(i):
                foo //= i
                bar *= i
                eggs.append(i)
                if is_prime(foo):
                    eggs.append(foo)
                break
    print(eggs)
    return max(eggs)


if __name__ == '__main__':
    tt = datetime.now()
    print(max_prime_factor(600851475143))
    print(datetime.now() - tt)
