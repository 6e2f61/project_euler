import pytest
from p003 import is_prime, max_prime_factor


@pytest.mark.parametrize('number', (
    2, 3, 5, 7, 11
))
def test_real_prime(number):
    assert is_prime(number)


@pytest.mark.parametrize('number', (
    -1, 0, 1, 4, 6, 8, 9, 10, 12
))
def test_not_prime(number):
    assert not is_prime(number)


@pytest.mark.parametrize('number, max_factorial', (
    (13195, 29),
    (147, 7),
    (13, 13)
))
def test_max_prime_factor(number, max_factorial):
    assert max_prime_factor(number) == max_factorial
