import pytest
from p007 import is_prime, find_nth_prime_number


@pytest.mark.parametrize("number, result", (
        (0, False),
        (1, False),
        (2, True),
        (3, True),
        (4, False),
        (5, True),
        (6, False),
        (7, True),
        (9, False),
        (10, False),
        (11, True),
        (15, False),
))
def test_if_is_prime(number, result):
    assert is_prime(number) is result


@pytest.mark.parametrize("index, value", (
        (1, 2),
        (2, 3),
        (3, 5),
        (4, 7),
        (5, 11),
        (6, 13),
        (10, 29),
        (20, 71),
))
def test_find_nth_prime_number(index, value):
    assert find_nth_prime_number(index) == value
